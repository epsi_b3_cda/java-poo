package epsi.software.models;

public class Square {
    private String symbol = "*";

    public void square(int number) {
        for (int i = 0; i < number; i++) {
            for (int j = 0; j < number; j++) {
                System.out.print(symbol);
            }
            System.out.println();
        }
    }
}