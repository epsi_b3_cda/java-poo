package epsi.software;

import epsi.software.models.Line;
import epsi.software.models.Square;
import epsi.software.models.Triangle;

import java.util.Scanner;

/**
 * Hello world!
 */
public class DisplayConsole {
    public static void main(String[] args) {
        int number = 5;
        String symbol = "=";
        System.out.println("LIGNE");
        Line line = new Line();

        line.line(5);

        System.out.println("\n");
        System.out.println("CARRE");
        Square square = new Square();

        square.square(5);

        System.out.println("TRIANGLE");
        Triangle triangle = new Triangle();
        for (int i = 0; i < number; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(symbol);
            }
            System.out.println("");
        }
    }
}
